# PHP

src repo: https://bitbucket.org/volodymyr_zhadlun/php

## 7.1
Using of official image: `php:7.1-fpm` 

Installed libraries: 
`nano`
`libssl-dev`
`libmcrypt-dev`
`libcurl4-openssl-dev`
`libmemcached-dev`
`zlib1g-dev`
`libxml2-dev`
`libicu-dev`

Installed extensions:
`memcached`
`mcrypt`
`mysqli`
`pdo_mysql`
`pdo`
`xmlrpc`
`opcache`
`json`
`xml`
`intl`
`pcntl`
`shmop`
`gd`
`igbinary`

## 7.1-dev
Using of image: `mtoy/php:7.1`

Installed libraries: 
`nodejs`
`git`
`zip`
`unzip`
`composer`
`gulp`
`yarn`

Installed extensions:
`xdebug`
`zip`

## 7.2
Using of official image: `php:7.2-fpm` 

Installed libraries: 
`nano`
`libssl-dev`
`libmcrypt-dev`
`libcurl4-openssl-dev`
`libmemcached-dev`
`zlib1g-dev`
`libxml2-dev`
`libicu-dev`

Installed extensions:
`mcrypt`
`memcached`
`mysqli`
`pdo_mysql`
`pdo`
`xmlrpc`
`opcache`
`json`
`xml`
`intl`
`pcntl`
`shmop`
`gd`
`igbinary`

## 7.2-cli
Using of official image: `php:7.2-cli` 

Installed libraries: 
`nano`
`libssl-dev`
`libmcrypt-dev`
`libcurl4-openssl-dev`
`libmemcached-dev`
`zlib1g-dev`
`libxml2-dev`
`libicu-dev`

Installed extensions:
`mcrypt`
`memcached`
`mysqli`
`pdo_mysql`
`pdo`
`xmlrpc`
`opcache`
`json`
`xml`
`intl`
`pcntl`
`shmop`
`gd`
`igbinary`
`pcntl`

## 7.2-dev
Using of image: `mtoy/php:7.2`

Installed libraries: 
`nodejs`
`git`
`zip`
`unzip`
`composer`
`gulp`
`yarn`

Installed extensions:
`xdebug`
`zip`